using System;

namespace TestingTask
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public string Department { get; set; }

        public DateTime DateofBirth { get; set; }
    }
}
