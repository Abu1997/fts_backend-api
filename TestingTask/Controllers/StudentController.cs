﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestingTask.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentController : ControllerBase
    {
      
        public StudentController()
        {
            
        }

        public static List<Student> data = new List<Student> { new Student { Id = 1, Name = "Asim", DateofBirth = DateTime.Parse("1999-01-16"), Gender = "Male", Department = "ECE" } };

        
        [HttpGet]
        [Route("api/Student/list")]
        public IActionResult Get()
        {
            return Ok(data);
        }

       
        [HttpGet]
        [Route("api/Student/Single/{Id}")]
        public IActionResult GetSingle(int Id)
        {
            return Ok(data[Id]);
        }


        [HttpPost]
        [Route("api/student/create")]
        public IActionResult Post([FromBody] Student values)
        {
            data.Add(values);
            return Ok();
        }

        [HttpPut]
        [Route("api/Student/Update")]
        public IActionResult Put([FromBody] Student values, int Id)
        {
            data[Id] = values;
            return Ok();
        }

        [HttpDelete]
        [Route("api/Student/Delete/{Id}")]
        public IActionResult Delete(int Id)
        {
            data.RemoveAt(Id);
            return Ok();
        }
    }
}
